# TeachTok - Valeriu Paloș

This is a straight-forward Expo application. It assumes a React-Native [development environment](https://reactnative.dev/docs/environment-setup?guide=quickstart).

```sh
# Running using Android emulator...
npm run android
```

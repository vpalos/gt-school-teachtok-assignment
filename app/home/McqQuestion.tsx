import React, { useState } from "react";
import { FlatList, Image, StyleProp, StyleSheet, TextStyle, TouchableHighlight } from "react-native";
import { Text, View } from "../../components/Themed";
import { Question, QuestionOption, QuestionType } from "../abstract/Question";

const CorrectGif = require("../../assets/images/correct.gif");
const IncorrectGif = require("../../assets/images/incorrect.gif");

const ANSWER_URI = "https://cross-platform.rp.devfactory.com/reveal?id=OPTION_ID";

export default ({ question }: { question: Question }) => {
  const [isAnswering, setIsAnswering] = useState(false);
  const [isResolved, setIsResolved] = useState(!!question.answer);

  async function getCorrectAnswer(): Promise<void> {
    if (!isAnswering && !isResolved) {
      setIsAnswering(true);
      const response = await fetch(ANSWER_URI.replace("OPTION_ID", String(question.id)));
      question.answer = await response.json();
      setIsResolved(true);
      setIsAnswering(false);
    }
  }

  const Option = ({ option }: { option: QuestionOption }) => {
    const [isPressed, setIsPressed] = useState(!!option.isPressed);

    async function onPress() {
      if (!isAnswering && !isResolved) {
        setIsPressed(true);
        option.isPressed = true;
        await getCorrectAnswer();
      }
    }

    const optionStyles: StyleProp<TextStyle> = [styles.common, styles.option];
    const isCorrect = question.answer?.correct_options[0].id === option.id;
    if (isResolved) {
      optionStyles.push(styles.resolvedOption);
      if (isCorrect || isPressed) {
        optionStyles.push(isCorrect ? styles.correctOption : styles.incorrectOption);
      }
    }

    return (
      <TouchableHighlight onPress={onPress} activeOpacity={0.75} underlayColor={"transparent"} disabled={isResolved}>
        <View style={optionStyles}>
          <Text style={[styles.common, styles.optionText]}>{option.answer}</Text>
          <View style={[styles.common, styles.indicator]}>
            {isResolved ? (
              isCorrect ? (
                <Image source={CorrectGif} style={styles.correctIndicator} />
              ) : isPressed ? (
                <Image source={IncorrectGif} style={styles.incorrectIndicator} />
              ) : null
            ) : null}
          </View>
        </View>
      </TouchableHighlight>
    );
  };
  return (
    <View style={[styles.common, styles.container]}>
      {/* Question */}
      <View style={[styles.common, styles.question]}>
        <Text style={[styles.common, styles.questionText]}>{question.question}</Text>
      </View>

      {/* Options */}
      <FlatList
        style={[styles.common, styles.options]}
        data={question.options}
        renderItem={({ item }) => <Option option={item} />}
        keyExtractor={(item) => String(item.id)}
      />

      {/* Details */}
      <View style={[styles.common, styles.brief]}>
        <Text style={[styles.common, styles.author]}>{question.user.name}</Text>
        <Text style={[styles.common]}>{question.description}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  common: {
    backgroundColor: "transparent",
    alignItems: "center",
    fontFamily: "SFProRounded",
  },

  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "stretch",
  },

  question: {
    display: "flex",
    justifyContent: "center",
    flexGrow: 0.5,
  },
  questionText: {
    alignSelf: "flex-start",
    width: "85%",
    backgroundColor: "rgba(0, 0, 0, 0.60)",
    fontSize: 22,
    fontWeight: "500",
    padding: 6,
    borderRadius: 8,
    lineHeight: 26,
  },

  options: {
    flexGrow: 1,
    width: "100%",
    alignItems: "stretch",
    justifyContent: "flex-end",
    gap: 8,
    marginBottom: 24,
  },
  option: {
    display: "flex",
    flexDirection: "row",
    gap: 5,
    position: "relative",
    backgroundColor: "rgba(255, 255, 255, 0.50)",
    borderRadius: 8,
    alignItems: "center",
    paddingLeft: 12,
    paddingRight: 4,
  },
  optionText: {
    paddingVertical: 12,
    fontSize: 17,
    flexGrow: 1,
    flexShrink: 1,
    fontWeight: "500",
    lineHeight: 18,
    textShadowColor: "rgba(0, 0, 0, 0.85)",
    textShadowOffset: { width: 1, height: 1.5 },
    textShadowRadius: 2,
  },
  resolvedOption: {
    backgroundColor: "rgba(255, 255, 255, 0.25)",
  },
  correctOption: {
    backgroundColor: "rgba(40, 177, 143, 0.50)",
  },
  incorrectOption: {
    backgroundColor: "rgba(220, 95, 95, 0.70)",
  },
  indicator: {
    width: 35,
    flexGrow: 0,
    flexShrink: 0,
  },
  correctIndicator: {
    width: 35,
    height: 35,
    marginBottom: 6,
  },
  incorrectIndicator: {
    width: 35,
    height: 35,
    marginTop: 6,
  },

  brief: {
    alignItems: "flex-start",
    fontSize: 13,
  },
  author: {
    fontSize: 15,
    fontWeight: "700",
  },
});

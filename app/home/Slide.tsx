import { LinearGradient } from "expo-linear-gradient";
import React, { Fragment } from "react";
import { Dimensions, ImageBackground, StyleSheet } from "react-native";
import { Text, View } from "../../components/Themed";
import AddAvatarSvg from "../../components/icons/AddAvatarSvg";
import BookmarkSvg from "../../components/icons/BookmarkSvg";
import ChevronRightSvg from "../../components/icons/ChevronRightSvg";
import CommentSvg from "../../components/icons/CommentSvg";
import HeartSvg from "../../components/icons/HeartSvg";
import PlaylistSvg from "../../components/icons/PlaylistSvg";
import ShareSvg from "../../components/icons/ShareSvg";
import { Question, QuestionType } from "../abstract/Question";
import McqQuestion from "../home/McqQuestion";

type SlideProps = {
  question: Question;
};

const TABBAR_HEIGHT = 49;
const PAGE_HEIGHT = Dimensions.get("window").height - TABBAR_HEIGHT;

export default ({ question }: SlideProps) => {
  const Badge = ({ icon, value }: { icon: React.ReactNode; value: number }) => {
    return (
      <View style={[styles.common, styles.badge]}>
        {icon}
        <Text style={[styles.common, styles.badgeValue]}>{value}</Text>
      </View>
    );
  };

  const renderQuestion = () => {
    switch (question.type) {
      case QuestionType.MCQ:
        return <McqQuestion question={question} />;
      default:
        return <Fragment></Fragment>;
    }
  };

  return (
    <View style={{ height: PAGE_HEIGHT }}>
      <ImageBackground source={{ uri: question.image }} resizeMode="cover" style={[styles.common, styles.image]}>
        <LinearGradient colors={["rgba(0, 0, 0, 0.45)", "rgba(0, 0, 0, 0.45)"]} style={[styles.common, styles.shade]} />

        <View style={[styles.common, styles.page]}>
          <View style={[styles.common, styles.left]}>{renderQuestion()}</View>

          {/* Badges */}
          <View style={[styles.common, styles.right]}>
            <AddAvatarSvg avatar={question.user.avatar} />
            <Badge icon={<HeartSvg />} value={87}></Badge>
            <Badge icon={<CommentSvg />} value={2}></Badge>
            <Badge icon={<BookmarkSvg />} value={203}></Badge>
            <Badge icon={<ShareSvg />} value={17}></Badge>
          </View>
        </View>
      </ImageBackground>

      <View style={[styles.common, styles.playlist]}>
        <PlaylistSvg />
        <Text style={[styles.common, styles.playlistText]}>{question.playlist}</Text>
        <ChevronRightSvg />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  common: {
    backgroundColor: "transparent",
    alignItems: "center",
    fontFamily: "SFProRounded",
  },

  container: {
    flex: 1,
    position: "relative",
    alignItems: "stretch",
  },
  image: {
    flex: 1,
    paddingTop: 99,
    justifyContent: "flex-start",
  },
  shade: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },

  page: {
    flex: 1,
    flexDirection: "row",
    alignItems: "stretch",
    marginBottom: 16,
    paddingLeft: 16,
    paddingRight: 8,
    gap: 12,
  },

  left: {
    flex: 1,
    flexGrow: 1,
    flexShrink: 1,
    flexDirection: "column",
    alignItems: "stretch",
  },
  right: {
    flexGrow: 0,
    flexShrink: 0,
    flexDirection: "column",
    justifyContent: "flex-end",
    width: 45,
    gap: 15,
  },
  badge: {
    gap: 5,
    flexDirection: "column",
    width: 45,
  },
  badgeValue: {
    fontSize: 12,
    fontWeight: "500",
    lineHeight: 12,
  },

  playlist: {
    backgroundColor: "#161616",
    display: "flex",
    flexDirection: "row",
    gap: 4,
    padding: 16,
  },
  playlistText: {
    flex: 1,
    fontSize: 13,
    lineHeight: 13,
    fontWeight: "600",
  },
});

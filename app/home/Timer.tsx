import { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import TimerSvg from "../../components/icons/TimerSvg";

const startTime = Date.now();

export default ({}) => {
  const [timer, setTimer] = useState(startTime);

  useEffect(() => {
    setTimeout(() => setTimer(Date.now()), 1000);
  }, [timer]);

  const seconds = Math.round((timer - startTime) / 1000);
  const minutes = Math.round(seconds / 60);
  const elapsedTime = seconds < 60 ? `${seconds}s` : `${minutes}m`;

  return (
    <View style={styles.timer}>
      <TimerSvg />
      <Text style={styles.timerText}>{elapsedTime}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  timer: {
    backgroundColor: "transparent",
    alignItems: "center",
    display: "flex",
    flexDirection: "row",
    gap: 4,
    opacity: 0.6,
  },
  timerText: {
    fontFamily: "SFProRounded",
    fontSize: 14,
    fontWeight: "400",
    lineHeight: 14,
    color: "#ffffff",
  },
});

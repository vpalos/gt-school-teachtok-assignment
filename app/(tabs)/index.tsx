import React, { useEffect, useState } from "react";
import { Image, StyleSheet, VirtualizedList } from "react-native";
import { Text, View } from "../../components/Themed";
import SearchSvg from "../../components/icons/SearchSvg";
import { Question } from "../abstract/Question";
import Slide from "../home/Slide";
import Timer from "../home/Timer";

const QUESTION_URI = "https://cross-platform.rp.devfactory.com/for_you";

const Questions: Question[] = [];

export default function HomeScreen() {
  const [isLoading, setIsLoading] = useState(false);

  async function loadNewQuestion() {
    if (!isLoading) {
      setIsLoading(true);

      // Sometimes the API returns the same question.
      let retries = 10;
      while (retries-- > 0) {
        const response = await fetch(QUESTION_URI);
        const question: Question = await response.json();

        // If new question, add to stack and preload resources (in parallel).
        if (Questions.every((_) => _.id !== question.id)) {
          Questions.push(question);
          await Promise.allSettled([Image.prefetch(question.image), Image.prefetch(question.user.avatar)]);
          break;
        }
      }
      setIsLoading(false);
    }
  }

  useEffect(() => {
    // Initial slides.
    loadNewQuestion();
    loadNewQuestion();
  }, []);

  return (
    <View style={[styles.common, styles.container]}>
      <VirtualizedList
        contentContainerStyle={{ flexGrow: 1 }}
        data={Questions}
        getItemCount={(data: any) => data.length}
        getItem={(data: any, index: number) => data[index]}
        pagingEnabled={true}
        initialNumToRender={1}
        windowSize={3}
        snapToAlignment={"start"}
        decelerationRate={"fast"}
        showsVerticalScrollIndicator={false}
        renderItem={({ item }) => <Slide question={item} />}
        keyExtractor={(item) => String(item.id)}
        onEndReached={loadNewQuestion}
        onEndReachedThreshold={1}
        removeClippedSubviews={true}
      />

      <View style={[styles.common, styles.header]}>
        <View style={[styles.common, { width: "25%", alignItems: "flex-start" }]}>
          <Timer />
        </View>

        <View style={[styles.common, { width: "50%" }]}>
          <Text style={[styles.common, styles.title]}>For You</Text>
          <View style={[styles.common, styles.underline]}></View>
        </View>

        <View style={[styles.common, { width: "25%", alignItems: "flex-end" }]}>
          <SearchSvg />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  common: {
    backgroundColor: "transparent",
    alignItems: "center",
    fontFamily: "SFProRounded",
  },

  container: {
    flex: 1,
    position: "relative",
    alignItems: "stretch",
  },

  header: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    position: "absolute",
    paddingHorizontal: 16,
    marginTop: 44,
    width: "100%",
  },
  title: {
    textAlign: "center",
    fontSize: 16,
    fontWeight: "600",
    lineHeight: 22,
  },
  underline: {
    marginTop: 5,
    backgroundColor: "white",
    width: 30,
    height: 4,
  },
});

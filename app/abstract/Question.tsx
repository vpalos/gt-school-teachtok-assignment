export enum QuestionType {
  MCQ = "mcq",
}

export type Question = {
  // API data.
  type: QuestionType;
  id: number;
  playlist: string;
  description: string;
  question: string;
  image: string;
  options: QuestionOption[];
  user: QuestionAuthor;

  // Stored response.
  answer?: CorrectAnswer;
};

export type QuestionOption = {
  // API data.
  id: string;
  answer: string;

  // State data.
  isPressed?: boolean;
};

export type QuestionAuthor = {
  name: string;
  avatar: string;
};

export type CorrectAnswer = {
  id: string;
  correct_options: { id: string; answer: string }[];
};

import * as React from "react";
import Svg, { Circle, Defs, G, Image, Path, Pattern, Rect, Use } from "react-native-svg";

export default function AddAvatarSvg({ avatar }: { avatar: string }) {
  return (
    <Svg width={49} height={54} fill="none">
      <G filter="url(#a)" shapeRendering="crispEdges">
        <Circle cx={23.5} cy={23.5} r={22.5} fill="url(#b)" />
        <Circle cx={23.5} cy={23.5} r={22} stroke="#fff" />
      </G>
      <Rect width={22} height={22} x={12.5} y={32} fill="#28B18F" rx={11} />
      <Path
        fill="#fff"
        d="M18.473 42.77c0-.487.398-.885.89-.885h3.252v-3.252c0-.486.393-.89.885-.89s.89.404.89.89v3.252h3.253a.888.888 0 0 1 0 1.775H24.39v3.252c0 .486-.399.89-.891.89a.889.889 0 0 1-.885-.89V43.66h-3.252a.89.89 0 0 1-.89-.89Z"
      />
      <Defs>
        <Pattern id="b" width={1} height={1} patternContentUnits="objectBoundingBox">
          <Use xlinkHref="#c" transform="scale(.0025)" />
        </Pattern>
        <Image href={avatar} id="c" width={400} height={400} />
      </Defs>
    </Svg>
  );
}
